﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public float maxDistance;
    public LayerMask mask;

    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public float hitForce;
    public float hitDamage;

    public bool isShooting;
    public bool isReloading;

    public float reloadTime;

    private LineRenderer laserLine;

    public GameObject explosion;

    private void Start()
    {
        isShooting = false;
        isReloading = false;
        currentAmmo = maxAmmo;
        laserLine = GetComponent<LineRenderer> ();
        hitForce = 100;
    }

    public void Shoot()
    {
        if(isShooting || isReloading) return;
        if(currentAmmo <= 0) return;

        Debug.Log ("Shoot");

        isShooting = true;        
        currentAmmo--;

        Ray ray = Camera.main.ViewportPointToRay (new Vector3(0.5f, 0.5f, 0.0f));


        // Set the start position for our visual effect for our laser to the position of gunEnd
        laserLine.enabled = true;
        laserLine.SetPosition (0, Camera.main.transform.position);

        RaycastHit hit;

        if (Physics.Raycast (ray, out hit, maxDistance,mask)){
            Debug.Log ("Hit");
            Debug.Log (hit.transform.name);
            if(hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            //laserLine.SetPosition (1, hit.transform.position);

            GameObject go = Instantiate(explosion);
            go.transform.position = hit.transform.position;

        } else {
            //Transform t = Camera.main.transform;
            //t.Translate(ray.direction * 100, Space.World);
            //Debug.DrawRay(ray.origin, Camera.main.transform.forward * 100, Color.green); 
            //laserLine.SetPosition (1, Camera.main.transform*ray.direction*100);
        }
        
        StartCoroutine(WaitFireRate());
    }
    private IEnumerator WaitFireRate()
    {/*
        float timeCounter = 0;
        while(timeCounter < fireRate)
        {
            timeCounter += Time.deltaTime;
        }
        isShooting = false;

        yield return null;*/
        Debug.Log("Empieza la corutina");
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        laserLine.enabled = false;
        Debug.Log("Termina la corutina");
    }

    public void Reload()
    {
        if(isReloading) return;
        isReloading = true;

        StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);

        currentAmmo = maxAmmo;
        isReloading = false;
    }
}