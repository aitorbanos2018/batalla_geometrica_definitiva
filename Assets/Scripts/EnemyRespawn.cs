﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRespawn : MonoBehaviour {

	public GameObject creeper;
	public Transform[] points;
	public float timeToSpawn;

	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);

			GameObject icreeper = Instantiate (creeper);
			ExplosiveEnemy cb = icreeper.GetComponent<ExplosiveEnemy> ();
			cb.pathNodes = points;
		}
	}
}