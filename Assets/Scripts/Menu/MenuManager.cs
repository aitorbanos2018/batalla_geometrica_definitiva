﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
     public void PulsaPlay()
    {
        SceneManager.LoadScene("GamePlay");
    }
    public void PulsaExit()
    {
        Application.Quit();
    }
    public void PulsaCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void PulsaMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
    public void PulsaControls()
    {
        SceneManager.LoadScene("ControlScene");
    }
}
